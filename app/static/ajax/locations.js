{"locations":
	[{
		"propertyId": 0,
		"type": 1,
		"name": "The Nest",
		"address": "koramangala, Koramangala 4th Block, Bangalore - 560034, Behind Oasis Mall",
		"price": "8000",
		"location": [12.9539974,77.6309395],
		"cover": "static/img/map-card.jpg",
		"url": "single.html"
	},
	{
	    "propertyId": 1,
		"type": 2,
		"name": "The Nest",
		"address": "koramangala, Koramangala 4th Block, Bangalore - 560034, Behind Oasis Mall",
		"price": "8000",
		"location": [12.9797999,77.7499467],
		"cover": "static/img/map-card.jpg",
		"url": "single.html"
	},
	{
	    "propertyId": 2,
		"type": 1,
		"name": "The Nest",
		"address": "koramangala, Koramangala 4th Block, Bangalore - 560034, Behind Oasis Mall",
		"price": "8000",
		"location": [12.9797999,77.8499467],
		"cover": "static/img/map-card.jpg",
		"url": "single.html"
	}]
}