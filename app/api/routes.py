from flask import request, jsonify
from . import api

from ..models import Common_facility,Partner,Pg,Room,User
from ..common import list_of_partners

@api.errorhandler(404)
def not_found(error=None):
    message = {
            'status': 404,
            'message': 'Not Found: ' + request.url,
    }
    resp = jsonify(message)
    resp.status_code = 404
    return resp

@api.route('/partners/', methods = ['GET'])
def get_partners_detail():
    data = {
        'partners': list_of_partners(),
    }

    if data:
        resp = jsonify(data)
        resp.status_code = 200
        return resp
    else:
        return not_found()

@api.route('/pgs/', methods = ['GET'])
def get_pg_list():
    #locality = request.args['locality']
    called_from = request.args.get('called_from')
    print ("This page is called from" , called_from)
    data = { "locations":
        [{
            "propertyId": 0,
            "type": 1,
            "name": "The Nest",
            "locality":"Koramangala",
            "address": "koramangala, Koramangala 4th Block, Bangalore - 560034, Behind Oasis Mall",
            "price": 8000,
            "location": [12.9539974,77.6309395],
            "cover": "static/img/map-card.jpg",
            "url": "single.html"
        },
        {
            "propertyId": 1,
            "type": 2,
            "name": "The Nest",
            "locality":"Whitefield",
            "address": "koramangala, Koramangala 4th Block, Bangalore - 560034, Behind Oasis Mall",
            "price": 10000,
            "location": [12.9797999,77.7499467],
            "cover": "static/img/map-card.jpg",
            "url": "single.html"
        },
        {
            "propertyId": 2,
            "type": 1,
            "name": "The Nest",
            "locality":"BTM",
            "address": "koramangala, Koramangala 4th Block, Bangalore - 560034, Behind Oasis Mall",
            "price": 12000,
            "location": [12.9797999,77.8499467],
            "cover": "static/img/map-card.jpg",
            "url": "single.html"
        }]
    }

    if data:
        resp = jsonify(data)
        resp.status_code = 200
        return resp
    else:
        return not_found()


