from flask import render_template, flash, redirect, url_for, request, jsonify, current_app
from . import admin

from .. import db
from ..models import Common_facility,Partner,Pg,Room,User
from flask.ext.login import login_user, logout_user, current_user, login_required
from ..common import create_or_update_partner, add_or_edit_pg, add_or_edit_rooms_for_pg, add_or_edit_facilities_for_pg, allowed_file
from werkzeug import secure_filename
import os
import errno

@admin.route('/loginpage', methods = ['GET','POST'])
def admin_loginpage():
    next = request.args.get('next')
    return render_template('login.html',
                           login_facebook= '/admin/login/facebook',
                           login_google= '',
                           next = next,
                           role = 'admin')

@admin.route('/authorize/', methods = ['GET','POST'])
def admin_authorize():
    return '<h1>Authorized</h1>'

@admin.route('/login/<logintype>/', methods = ['GET','POST'])
def admin_login(logintype):
    return redirect(url_for('auth.oauth_authorize', called_from=request.url, provider=logintype))

@admin.route('/logout/', methods = ['GET','POST'])
def admin_logout():
    logout_user()
    return redirect(url_for('public_pages.index'))

@admin.route('/', methods = ['GET','POST'])
@admin.route('/profile/', methods = ['GET','POST'])
#@login_required
def admin_profile():
    return render_template('admin/profile.html')


@admin.route('/partner_update/', methods = ['GET','POST'])
#@login_required
def partner_update_or_create():
    partner = {}
    form = request.form
    file = request.files['partner_photo_upload']

    if form.get('partner_id'):
        partner['id'] = form['partner_id']

    partner['name'] = form.get('partner_name')
    partner['email'] = form.get('partner_email')
    partner['phone_number'] = form.get('partner_phone_number')
    partner['about'] = form.get('partner_description')
    partner['website'] = form.get('partner_website')
    #partner['type'] = form.get('partner_type')
    partner_id = create_or_update_partner(partner)
    if partner_id and file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        dir = os.path.join(current_app.static_folder, current_app.config['PARTNER_PHOTO_UPLOAD_DIR'], str(partner_id))
        try:
            os.makedirs(dir)
        except OSError as exception:
            if exception.errno != errno.EEXIST:
                raise

        file_path = os.path.join(dir, filename)
        file.save(file_path)

        pat = Partner.query.get(partner_id)
        pat.photo = filename
        db.session.merge(pat)
        db.session.commit()

    flash("Partner updated successfully")
    return redirect(url_for('admin.admin_profile'))

@admin.route('/pg_update_or_create/', methods = ['POST'])
#@login_required
def pg_update_or_create():
    form = request.form
    pg = {}
    rooms ={}
    facilities = {}
    pg['id'] = form['pgid']
    pg['partner_id'] = form['partnerid']
    pg['type'] = form['pg_type']
    pg['phone_number'] = form['pg_phone_number']
    pg['name'] = form['pg_name']
    pg['locality'] = form['pg_locality']
    pg['lat'] = form['pg_lat']
    pg['long'] = form['pg_long']
    pg['single_sharing'] = form['pg_single_sharing']
    pg['double_sharing'] = form['pg_double_sharing']
    pg['triple_sharing'] = form['pg_triple_sharing']
    pg['four_sharing'] = form['pg_four_sharing']
    pg['starting_rate'] = form['pg_starting_rate']
    pg['serving_gender'] = form['pg_serving_gender']
    pg['active'] = form['pg_active']
    pg['images'] = form['pg_images']

    rooms['id'] = form['room_id']
    rooms['pg_id'] = form['pgid']
    rooms['type'] = form['room_type']
    rooms['occupancy'] = form['room_occupancy']
    rooms['rates'] = form['room_rates']
    rooms['total_room_count'] = form['room_count']

    facilities['pg_id'] = form['pgid']
    facilities['wifi'] = form['wifi']
    facilities['security'] = form['room_type']
    facilities['reception'] = form['room_occupancy']
    facilities['cctv'] = form['room_rates']
    facilities['drinking_water'] = form['room_count']
    facilities['lift'] = form['room_id']
    facilities['parking'] = form['pgid']
    facilities['power_backup'] = form['room_type']
    facilities['tv'] = form['room_occupancy']
    facilities['laundry'] = form['room_rates']
    facilities['hot_water'] = form['room_count']
    facilities['almirah'] = form['room_id']
    facilities['house_keeping'] = form['pgid']
    facilities['pillow'] = form['room_type']
    facilities['breakfast'] = form['room_occupancy']
    facilities['lunch'] = form['room_rates']
    facilities['dinner'] = form['room_count']
    #facilities['date_of_addition'] = form['room_count']
    #facilities['date_of_last_modification'] = form['room_count']

    pg_id = add_or_edit_pg(pg)
    rooms['pg_id'] = pg_id
    add_or_edit_rooms_for_pg(rooms)
    add_or_edit_facilities_for_pg(facilities)
    return "PG updated successfully: " + pg_id






