from flask import render_template, flash, redirect, url_for, request, jsonify
from . import partners
from random import randint
from forms import register_form
from .. send_sms import send_sms
#randint(1001,9999)

from .. import db
from ..models import Appointment,Common_facility,Complain,Customer,Partner,Payment_History,Pg,Room,User
from flask.ext.login import login_user, logout_user, current_user, login_required

@partners.route('/register_partner/',methods = ['GET','POST'])
def register_partner():

    if request.method == "POST":
        flash("Welcome")
        if request.form['email']:
            print "Email is " + request.form['email']
            if request.form['password1'] == request.form['password2']:
                print "Password is " + request.form['password1']
                flash("Please verify your accpunt by logging into " , request.form['email'])
            else:
                flash("Password does not match please retry")
        else:
            flash("Please enter an email id")
    else:
        print "Not POST"
    #return redirect(url_for('customers.index'))
    return render_template('index.html')
    #form = register_form()
    #flash('Please enter your mobile number to register, we will SMS you the verification code')
    #if form.validate_on_submit():
    """if request.method == "POST":
        print "Inside POST"
        cell_number= request.form['UserMobile']
        random_number = randint(1001,9999)
        print "sending SMS to " , random_number
        sms = send_sms(cell_number,'Pikuhomes Password is : ' + str(random_number) + '\n\n\n\nContact Piku Technical Dept 9845204336 for technical issue.\n\n')
        if(sms.send() == 0):
            flash('Code is sent to your mobile number' + str(cell_number) + 'successfully, please enter the code')
            partner = Partner(phone_number  = cell_number, password = random_number)
            db.session.add(partner)
            db.session.commit()
            return render_template('partner_login.html')
        else:
            flash('We were unable to deliver you SMS. Please verify your cell number')
            flash('If you think its a system issue please call on 9845204336 to register')
    return render_template('index.html')"""

@partners.route('/partner-login/',methods = ['GET','POST'])
def login_partner():
    form = partner_login()
    if form.validate_on_submit():
        partner_email_from_db = User.query.filter_by(email=form.email.data).first()
        if partner_email_from_db is None:
            flash('This email does not exists, please register')
            return redirect(url_for('.new_partner'))
        elif not partner_email_from_db.verify_password(form.password.data):
            flash("invalid password")
            form.email = form.email.data
            return render_template('index.html', form = form)
        else:
            login_user(partner_email_from_db)
            return redirect (url_for('.show_my_pgs'))
    return render_template('index.html', form = form)

@partners.route('/partner/loginpage', methods = ['GET','POST'])
def partner_loginpage():
    next = request.args.get('next')
    return render_template('login.html',
                           login_facebook= '/partner/login/facebook',
                           login_google= '',
                           next = next,
                           role = 'partner')

@partners.route('/partner/authorize/', methods = ['GET','POST'])
def partner_authorize():
    return '<h1>Authorized</h1>'

@partners.route('/partner/login/<logintype>/', methods = ['GET','POST'])
def partner_login(logintype):
    return redirect(url_for('auth.oauth_authorize', called_from=request.url, provider=logintype))

@partners.route('/partner/logout/', methods = ['GET','POST'])
def partner_logout():
    logout_user()
    return redirect(url_for('public_pages.index'))

@partners.route('/partner/profile/', methods = ['GET','POST'])
@login_required
def partner_profile():
    return '<h2>partner profile</h2>'






