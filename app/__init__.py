from flask import Flask, url_for
from config import config
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.login import LoginManager
#bootstrap = Bootstrap()


db = SQLAlchemy()
lm = LoginManager()

def create_app(config_name):
    app = Flask(__name__)
    #bootstrap.init_app(app)
    db.init_app(app)
    lm.init_app(app)
    app.config.from_object(config[config_name])

    from .public_pages import public_pages as public_pages_blueprint
    app.register_blueprint(public_pages_blueprint)

    from .customers import customers as customer_blueprint
    app.register_blueprint(customer_blueprint)

    from .Partners import partners as partners_blueprint
    app.register_blueprint(partners_blueprint)

    from .admin import admin as admin_blueprint
    app.register_blueprint(admin_blueprint)

    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint)

    from .api import api as api_blueprint
    app.register_blueprint(api_blueprint)

    # Doc: http://flask-login.readthedocs.org/en/latest/#flask.ext.login.login_required
    lm.blueprint_login_views = {
        'admin'  : 'admin.admin_loginpage',
        'Partners'  : 'Partners.partner_loginpage',
        'customers' : 'customers.customer_loginpage'
    }

    return app
