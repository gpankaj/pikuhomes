from flask import render_template, flash, redirect, url_for, request, session
from . import auth

from .. import db, lm
from .. models import User

from flask.ext.login import login_user, logout_user, current_user
from .oauth import OAuthSignIn
from urlparse import urlparse, parse_qs

@lm.user_loader
def load_user(id):
    role = urlparse(request.url).path.split('/')[1]
    user_obj = User.query.get(int(id))

    if user_obj is not None and not user_obj.type == role:
        #flash("You are not authorized to access this URL.")
        return None
    return user_obj

@auth.route('/register/', methods = ['GET','POST'])
def register():
    if request.method == "POST":
        flash("Welcome")
    return render_template('index.html')

@auth.route('/login/', methods = ['GET','POST'])
def oauth_authorize():
    called_from = request.args.get('called_from')
    provider = request.args.get('provider')
    query_ps = parse_qs(urlparse(called_from).query, keep_blank_values=True)
    # e.g. query_ps will be  {u'role': [u'admin'], u'next': [u'/admin/profile/']}
    session['next_url'] = query_ps['next'][0]
    session['role'] = query_ps['role'][0]
    if not current_user.is_anonymous:
        return redirect(url_for('public_pages.index'))
    oauth = OAuthSignIn.get_provider(provider)
    return oauth.authorize()

@auth.route('/logout/', methods = ['GET','POST'])
def logout():
    logout_user()
    return redirect(url_for('public_pages.index'))


@auth.route('/callback/<provider>')
def oauth_callback(provider):
    next_url = session['next_url']
    role = session['role']
    if not current_user.is_anonymous:
        return redirect(url_for('public_pages.index'))
    oauth = OAuthSignIn.get_provider(provider)
    social_id, username, email = oauth.callback()

    if social_id is None:
        return redirect(url_for('public_pages.index'))
    user = User.query.filter_by(type=role, auth_type_uid=social_id, auth_type=provider).first()
    if not user:
        new_user = User(type=role, auth_type_uid=social_id, auth_type=provider, email=email)
        db.session.add(new_user)
        db.session.commit()
        login_user(new_user, True)
    else:
        login_user(user, True)
    return redirect(next_url)
