from flask import render_template, flash, redirect, url_for, request, jsonify, current_app, send_from_directory
from . import public_pages

from .. import db
#from ..models import Appointment,Common_facility,Complain,Customer,Partner,Payment_History
import os

@public_pages.route('/',methods = ['GET','POST'])
@public_pages.route('/index',methods = ['GET','POST'])
@public_pages.route('/index.html',methods = ['GET','POST'])
def index():
    return render_template('index.html')


@public_pages.route('/single',methods = ['GET','POST'])
@public_pages.route('/single.html',methods = ['GET','POST'])
def single_function():
    return render_template('single.html')


@public_pages.route('/subdomain/<page>',methods = ['GET','POST'])
def subdomain_redirect(page):
    return render_template('/subdomain/' + page)


@public_pages.route('/photo/partner/<int:partnerid>/<path:filename>')
def send_photo_partner(partnerid, filename):
    dir = os.path.join(current_app.config['PARTNER_PHOTO_UPLOAD_FOLDER'], str(partnerid))
    return send_from_directory(dir, filename)

@public_pages.route('/photo/pg/<int:pgid>/<path:filename>')
def send_photo_pg(pgid, filename):
    dir = os.path.join(current_app.config['PG_PHOTO_UPLOAD_FOLDER'], str(pgid))
    return send_from_directory(dir, filename)