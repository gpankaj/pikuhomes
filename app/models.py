from datetime import datetime
from flask import request
from . import db, lm
from werkzeug.security import generate_password_hash, check_password_hash
from flask.ext.login import UserMixin


class Partner(db.Model):
    __tablename__ = 'partners'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100), nullable=False)
    name = db.Column(db.String(512), nullable=True)
    phone_number = db.Column(db.String(10), nullable=True)
    about = db.Column(db.String(1000), nullable=True)
    website = db.Column(db.String(100), nullable=True)
    photo = db.Column(db.String(1000), nullable=True)  # Path of photo of S3 buack
    type = db.Column(db.String(100), nullable=True)
    date_of_creation = db.Column(db.DateTime, nullable=True)
    date_of_modification = db.Column(db.DateTime, nullable=True)
    user_accounts = db.Column(db.Integer, db.ForeignKey('user_accounts.id'), nullable=True)

    def as_dict(self):
        dict_ = {}
        for key in self.__mapper__.c.keys():
            dict_[key] = getattr(self, key)
        return dict_

    def __init__(self, partner):
        self.name = partner['name']
        self.email = partner['email']
        self.phone_number = partner['phone_number']
        self.about = partner['about']
        self.website = partner['website']
        #self.photo = partner['photo']
        #self.type = partner['type']


# below is basic information model which will help in quick search
class Pg(db.Model):
    __tablename__ = 'pgs'
    id = db.Column(db.Integer, primary_key=True)
    type = db.Column(db.Integer, nullable=False)  # in apartment or individuals house
    name = db.Column(db.String(512), nullable=False)
    locality = db.Column(db.String(512), nullable=False)
    lat = db.Column(db.Float, nullable=False)
    long = db.Column(db.Float, nullable=False)
    single_sharing = db.Column(db.Boolean, nullable=False)
    double_sharing = db.Column(db.Boolean, nullable=False)
    triple_sharing = db.Column(db.Boolean, nullable=False)
    four_sharing = db.Column(db.Boolean, nullable=False)
    starting_rate = db.Column(db.Integer)
    serving_gender = db.Column(db.String(16), nullable=False)
    phone_number = db.Column(db.String(64))
    active = db.Column(db.Boolean, default=True)
    images = db.Column(db.Text)  # convert to an array or json
    partner_id = db.Column(db.Integer, db.ForeignKey('partners.id'))

    def __init__(self, pg):
        self.partner_id = pg['partner_id']
        self.type = pg['type']
        self.phone_number = pg['phone_number']
        self.name = pg['name']
        self.locality = pg['locality']
        self.lat = pg['lat']
        self.long = pg['long']
        self.single_sharing = pg['single_sharing']
        self.double_sharing = pg['double_sharing']
        self.triple_sharing = pg['triple_sharing']
        self.four_sharing = pg['four_sharing']
        self.starting_rate = pg['starting_rate']
        self.serving_gender = pg['serving_gender']
        self.active = pg['active']
        self.images = pg['images']


class Common_facility(db.Model):
    __tablename__ = 'common_facilities'
    pg_id = db.Column(db.Integer, db.ForeignKey('pgs.id'), primary_key=True)
    wifi = db.Column(db.Boolean, nullable=False, default=False)
    security = db.Column(db.Boolean, nullable=False, default=False)
    reception = db.Column(db.Boolean, nullable=False, default=False)
    cctv = db.Column(db.Boolean, nullable=False, default=False)
    drinking_water = db.Column(db.Boolean, nullable=False, default=False)
    lift = db.Column(db.Boolean, nullable=False, default=False)
    parking = db.Column(db.Boolean, nullable=False, default=False)
    power_backup = db.Column(db.Boolean, nullable=False, default=False)
    tv = db.Column(db.Boolean, nullable=False, default=False)
    laundry = db.Column(db.Boolean, nullable=False, default=False)
    hot_water = db.Column(db.Boolean, nullable=False, default=False)
    almirah = db.Column(db.Boolean, nullable=False, default=False)
    house_keeping = db.Column(db.Boolean, nullable=False, default=False)
    pillow = db.Column(db.Boolean, default=True)
    breakfast = db.Column(db.String(24))
    lunch = db.Column(db.String(24))
    dinner = db.Column(db.String(24))
    description = db.Column(db.String(1024))
    date_of_addition = db.Column(db.DateTime(), default=datetime.utcnow)
    date_of_last_modification = db.Column(db.DateTime(), default=datetime.utcnow)

    def __init__(self, facilities):
        self.pg_id = facilities['pg_id']
        self.wifi = facilities['wifi']
        self.security = facilities['security']
        self.reception = facilities['reception']
        self.cctv = facilities['cctv']
        self.drinking_water = facilities['drinking_water']
        self.lift = facilities['lift']
        self.parking = facilities['parking']
        self.power_backup = facilities['power_backup']
        self.tv = facilities['tv']
        self.laundry = facilities['laundry']
        self.hot_water = facilities['hot_water']
        self.almirah = facilities['almirah']
        self.house_keeping = facilities['house_keeping']
        self.pillow = facilities['pillow']
        self.breakfast = facilities['breakfast']
        self.lunch = facilities['lunch']
        self.dinner = facilities['dinner']


class Room(db.Model):
    __tablename__ = 'rooms'
    id = db.Column(db.Integer, primary_key=True)
    pg_id = db.Column(db.Integer, db.ForeignKey('pgs.id'))
    type = db.Column(db.Integer)  # Room type i.e. single sharing means 1, double sharing means 2.. this also denotes capacity
    occupancy = db.Column(db.Integer)
    rates = db.Column(db.Integer)
    total_room_count = db.Column(db.Integer)
    description = db.Column(db.String(1000), nullable=True)

    def __init__(self, rooms):
        self.pg_id = rooms['pg_id']
        self.type = rooms['type']
        self.occupancy = rooms['occupancy']
        self.rates = rooms['rates']
        self.total_room_count = rooms['total_room_count']


class Customer(db.Model):
    __tablename__ = 'customers'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(512), nullable=False)
    phone_number = db.Column(db.Integer, nullable=False)
    email = db.Column(db.String(100), nullable=False)
    about = db.Column(db.String(1000))
    photo = db.Column(db.String(1000))  # Path of photo of S3 buack
    date_of_creation = db.Column(db.DateTime)
    date_of_start = db.Column(db.DateTime)
    date_of_end = db.Column(db.DateTime)
    date_of_modification = db.Column(db.DateTime)
    initial_payment = db.Column(db.Integer)
    rate_per_month = db.Column(db.Integer)
    other_charges_per_month = db.Column(db.Integer)
    user_accounts = db.Column(db.Integer, db.ForeignKey('user_accounts.id'))


class Payment_History(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id'))
    date_of_payment = db.Column(db.Integer)
    type = db.Column(db.String(100))  # initial deposit or monthly or something else.
    comments = db.Column(db.String(1000))


class Complain(db.Column):
    __tablename__ = 'complains'
    id = db.Column(db.Integer, primary_key=True)
    pg_id = db.Column(db.Integer, db.ForeignKey('pgs.id'))
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id'))
    type_of_complain = db.Column(db.String(500))
    description = db.Column(db.String(1000))
    status = db.Column(db.String(100))
    last_update_date = db.Column(db.DateTime)
    creation_date = db.Column(db.DateTime)


class Appointment(db.Model):
    __tablename__ = 'complains'
    id = db.Column(db.Integer, primary_key=True)
    pg_id = db.Column(db.Integer, db.ForeignKey('pgs.id'))
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id'))
    room_id = db.Column(db.Integer, db.ForeignKey('rooms.id'))
    description = db.Column(db.String(1000))


class User(UserMixin, db.Model):
    __tablename__ = 'user_accounts'
    id = db.Column(db.Integer, primary_key=True)
    type = db.Column(db.Enum('customer', 'partner', 'admin'))
    auth_type = db.Column(db.Enum('facebook', 'google', 'mobile', 'regular'))
    email = db.Column(db.String(100), nullable=True)
    password_hash = db.Column(db.Integer, nullable=True)
    auth_type_uid = db.Column(db.String(255), nullable=True)
    date_of_creation = db.Column(db.DateTime, nullable=True)
    created_by = db.Column(db.String(100), nullable=True)
    created_source = db.Column(db.String(50), nullable=True)  # Mobile, Desktop, etc.

    @property
    def password(self):
        raise AttributeError("You can't read the password. Don't try again.")

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def __init__(self, type, auth_type, auth_type_uid, email=None):
        self.type = type
        self.auth_type = auth_type
        self.auth_type_uid = auth_type_uid
        self.email = email.lower()
        self.date_of_creation = datetime.utcnow()

    # These four methods are for Flask-Login
    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)

