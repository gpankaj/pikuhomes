from .models import Partner, Pg, Room, Common_facility
from . import db
from flask import current_app

def create_or_update_partner(partner):
    pat = None
    if 'id' in partner:
        pat = Partner.query.get(partner['id'])
        pat.name = partner['name']
        pat.email = partner['email']
        pat.phone_number = partner['phone_number']
        pat.about = partner['about']
        pat.website = partner['website']
        db.session.merge(pat)
        db.session.commit()
    else:
        pat = Partner(partner)
        db.session.add(pat)
        db.session.commit()
    return pat.id

def add_or_edit_pg(pg):
    pg_obj = Pg(pg)
    db.session.add(pg_obj)
    db.session.commit()
    return pg_obj.id

def add_or_edit_rooms_for_pg(room):
    room_obj = Room(room)
    db.session.add(room_obj)
    db.session.commit()
    return room_obj.id

def add_or_edit_facilities_for_pg(facility):
    facility_obj = Common_facility(facility)
    db.session.add(facility_obj)
    db.session.commit()
    return facility_obj.pg_id

def list_of_partners():
    pats_obj = Partner.query.all()
    partners = [ pat.as_dict() for pat in pats_obj ]
    return partners

def list_of_pg_for_partner(partnerid):
    return 1

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1] in current_app.config['ALLOWED_PHOTO_EXTENSIONS']