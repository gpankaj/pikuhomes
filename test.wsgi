#!/usr/bin/python
import sys
import logging
import os
logging.basicConfig(stream=sys.stderr)
sys.path.insert(0,"/var/www/yaf/")

from flipflop import WSGIServer
from app import create_app 

os.environ['FLASK_CONFIG'] = 'production'
application = create_app(os.getenv('FLASK_CONFIG') or 'default')

if __name__ == '__main__':
    WSGIServer(application).run()
