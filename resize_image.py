from PIL import Image
from resizeimage import resizeimage
import glob
import os


# Pass inout file path , output file and desired size in pixels
# Please note we need to use PIL version 2.9 - Latest version of PIL does not work and has bug
# below is an example
def resize_file(in_file, out_file, size):
    with open(in_file,'r+b') as fd:
        with Image.open(fd) as image:
            cover = resizeimage.resize_cover(image, size)
            cover.save(out_file,image.format)
            image.close
            cover.close



"""
for filename in glob.glob(r'C:\Users\Desktop\Desktop\flask\pikuhomes\app\static\img\pg1\*.jpg'):
    print "Resizing " + filename
    basename = os.path.basename(filename)
    dirname = os.path.dirname(filename)
    resize_file(filename, basename, (420, 218))
"""

