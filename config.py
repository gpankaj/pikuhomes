import os
basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    SECRET_KEY = os.environ.get('SECRET_KEY') or 't0p s3cr3t'
    DEBUG = True

class DevelopmentConfig(Config):
    SQLALCHEMY_DATABASE_URI = os.environ.get('DEV_DATABASE_URL') or 'mysql://root:root@localhost/piku1'
    PARTNER_PHOTO_UPLOAD_DIR = 'img/uploads/partner'
    PG_PHOTO_UPLOAD_DIR = 'img/uploads/pg'
    ALLOWED_PHOTO_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])
    OAUTH_CREDENTIALS = {
        'facebook': {
            'id': '1704839693081785',
            'secret': '70953713cac9631ae5970f48f7a87d18'
        },
        'google': {
            'id': '',
            'secret': ''
        }
    }

class ProductionConfig(Config):
    SQLALCHEMY_DATABASE_URI = os.environ.get('DEV_DATABASE_URL') or 'mysql://pikuhomes:pikuhomes@pikuhomesprod.cd3pupo3w3sq.ap-southeast-1.rds.amazonaws.com:3306/pikuhomesprod'
    PARTNER_PHOTO_UPLOAD_DIR = 'img/uploads/partner'
    PG_PHOTO_UPLOAD_DIR = 'img/uploads/pg'
    OAUTH_CREDENTIALS = {
        'facebook': {
            'id': '1704656309766790',
            'secret': '30494764ab54cd4be7f51031b3d49b96'
        },
        'google': {
            'id': '',
            'secret': ''
        }
    }


config = {
    'development': DevelopmentConfig,
    'production': ProductionConfig,
    'default': DevelopmentConfig
}

